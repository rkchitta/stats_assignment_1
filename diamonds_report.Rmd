---
title: "Diamonds"
author: "Shubham Luthra, Rahul Chitta, Sandhya Manasa Gunda"
date: '2020-02-04'
output:
  pdf_document: default
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
library(ggplot2)
library(dplyr)
knitr::opts_chunk$set(echo = TRUE)
```


# 1. Data
We are analysing the dataset of diamonds, which is found in the ggplot2 package. The dataset contains the prices and other attributes of almost 54,000 diamonds. The data frame with 53940 rows and 10 variables:

1. **price** *is an integer vector that gives the monetary value in US dollars. It is a continuous variable.*
2. **carat** *is a numeric vector that gives the weight of the diamond. (0.2–5.01). It is a continuous variable.*
3. **cut** *is an ordered factor that gives the quality of the cut. It is divided into 5 categories : Fair, Good, Very Good, Premium, Ideal. It is an ordinal variable with ordering: Fair < Good < Very Good < Premium < Ideal.*
4. **color** *is an ordered factor that gives the color of the diamond. It is divided into 4 categories. It is a ordinal variable with ordering : D < E < F < G < H < I < J.*
5. **clarity** *is an ordered factor that gives the measurement of how clear the diamond is. It is divided into 4 categories : I1 (worst), SI2, SI1, VS2, VS1, VVS2, VVS1, IF (best). It is a ordinal variable with ordering : I1 < SI2 < SI1 < VS2 < VS1 < VVS2 < VVS1 < IF (best).*
6. **x** *is a numeric vector that gives the length of the diamond in mm. (0–10.74). It is a continuous variable.*
7. **y** *is a numeric vector that gives the width of the diamond in mm. (0–58.9) It is a continuous variable.*
8. **z** *is a numeric vector that gives the depth of the diamond in mm. (0–31.8) It is a continuous variable.*
9. **depth** *is a numeric vector that gives the total depth percentage which is calculated by the formula -  z / mean(x, y) = 2 times z / (x + y). It is a continuous variable.*
10. **table** *is a numeric vector that gives the width of top of diamond relative to widest point. It is a continuous variable.*


### Summary Statistics


```{r echo=FALSE}
smalldiamonds <- diamonds %>% select(carat,cut,color,clarity,price)
summary(smalldiamonds)
```



```{r fig.height=3.3,fig.width=3.3, echo=FALSE}
ggplot(data = diamonds) +
  geom_bar(mapping = aes(x = cut),fill = "#3CB371")

ggplot(data = diamonds) +
  geom_bar(mapping = aes(x = clarity),fill = "#3CB371")
```

```{r fig.height=3.3,fig.width=3.3, echo=FALSE}
ggplot(data = diamonds) +
  geom_bar(mapping = aes(x = color),fill = "#3CB371")
```


* For this dataset, we would be focussing only on the 4Cs (carat, cut, color, clarity) and their relation with the price. The price starts from $326 and goes upto $18823. The mean price is **$3933**, whereas the median price is **$2401**. The difference in the mean and median price indicates the skewness in the price distribution. The outliers (diamonds with carat>3), having an unusually high price, collectively increase the mean price to $3933. The range of the price is **$18497**.

* We have 741 diamonds with I1 clarity (worst clarity) and 1790 diamonds with the IF clarity (best clarity). Furthermore, we have 6775 diamonds with color D (best color) and 2808 diamonds with the color J (worst color).

* As far as the cut is concerned, the fair cut diamonds are significantly less in number than the ideal cut diamonds. The number of fair cut and ideal cut diamonds are 1610 and 21551 respectively.

* The minimum carat value is 0.20 and the maximum carat value is 5.01. For carat, the mean and median values are not as far as they are for the price. The mean is 0.7979ct and the median is 0.70ct. The range for the carat variable is 5.01 - 0.20= 4.81ct.

* Needless to say, price is the most important variable amongst all, as we would want to find correlations between the price and the other variables (carat, color, cut, clarity).



# 2. Analysis

```{r echo=FALSE}
ggplot(data=diamonds, mapping= aes(x=carat,y=price)) + geom_point(alpha=0.01)
```

* **We notice here that diamond price increases almost exponentially with diamond carat weight.** The prize bumps up every time the diamond hits its critical weight value. Critical weights, or what is often known as the "magic sizes" are a handful of whole number and fractional values of the carats, which are fairly common, and have a high cost because the diamonds with these carat values are in demand.

* Moreover, **we observe that the plot is a lot more denser around the smaller carat weight values.** The reason for this is that the diamonds with the carat weight of 0.50ct-1.50ct are much more common, have a high demand, and fit the pocket of an average potential customer.



```{r echo=FALSE}
dfiltered <- diamonds %>% filter(carat<4)
ggplot(data = dfiltered, mapping = aes(x = carat)) +
  geom_histogram(binwidth = 0.01,fill="#4682B4")
```

* **There are more diamonds at whole carats (1.00ct, 2.00ct, 3.00ct) and a few common fractions (0.30ct, 0.40ct, 0.50ct, 0.70ct, 0.90ct, 1.50ct)** because these are considered to be critical weights. Often called the "magic sizes", the diamonds with such weights are usually more in demand, and end up being a lot more expensive than the random fraction carat diamonds.

* Another interesting observation is that **there are more diamonds slightly to the right of each peak than there are slightly to the left of each peak.** As is established, the price bumps up, once a diamonds hits its critical weight. For this reason, the diamond cutters put every possible effort to keep the diamond weight at or above the its magic size. The demand for such sizes is a lot higher than the "off size diamonds" and hence, they are easily available.

* **Out of the 53,934 diamonds, only 32 diamonds (~0.6%) are more than 3 carats** simply because of the fact that diamonds with such large sizes are rare, and in turn, are much more expensive.



* **Another surprising observation in this dataset is that the diamonds with poor quality (poor cut, clarity, and color) are priced relatively higher than the one with good quality.**

```{r echo=FALSE, fig.height=4.5,fig.width=4.5}
ggplot(data=diamonds, mapping = aes(x=clarity,y=price,color=clarity)) + geom_boxplot()
ggplot(data=diamonds, mapping = aes(x=color,y=price,color=color)) + geom_boxplot()
```




```{r , echo=FALSE,fig.height=4.5,fig.width=4.8}
ggplot(data=diamonds, mapping = aes(x=cut,y=price,color=cut)) + geom_boxplot()
```

All the 4Cs must be taken into account when evaluating the correlation with the price. In the diamonds dataset, *the diamonds with the lower quality tend to be of larger size*. We must not forget about the 4th C here - the price, which is perhaps the most important while estimating the price of the diamonds.

For an instance, *the diamonds with the worst clarity (I1) are much larger than the diamonds with the best clarity (IF)*, and hence, are more expensive than the IF clarity diamonds.

```{r echo=FALSE,fig.width=4.2,fig.height=3.5}
worstclardiamonds<-diamonds %>% filter(clarity=="I1")
bestclardiamonds <- diamonds %>% filter(clarity=="IF")
ggplot(data=worstclardiamonds, mapping= aes(x=carat,y=price)) + xlab("carat (for I1 clarity)")  +geom_point()
ggplot(data=bestclardiamonds, mapping= aes(x=carat,y=price)) + xlab("carat (for IF clarity)") +geom_point()
```

# 3. Conclusion

### Key Takeaways

* With only a bit of knowledge about the magic sizes and understanding of the price-carat trend, you can save plenty of money while diamond shopping. There is almost no visual difference between a 1.99 ct diamond and a 2.00ct diamond, but since the price increases exponentially at each of the critical weights, a 2.00ct diamond could be far more expensive than a 1.99ct diamond.

* All the 4Cs (color, cut, clarity, carat) are equally important for a good quality diamond. With a price constraint, buying a larger carat size diamond could seem like a lucrative decision, but the trade-off with the other 3Cs would essentially mean in fact a poor overall quality. To stay within the budget and to get a good quality diamond, keep in mind to keep all 4Cs in a balanced proportion.


